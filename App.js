import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Index from './navigation';

import Test from './src/screens/homeScreen'
import {AppRegistry} from "react-native";
import {Provider} from "react-redux";
import {name as appName} from "./app.json";
import store from "./src/redux/stores/store";

export default function App() {
  return (
    <Provider store={store()}>
      <Index/>
    </Provider>
    // <View style={styles.container}>
    //   <StatusBar style="auto" />
    //   <Test/>
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    marginTop:20
  }
});
