import React from 'react';
import { NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import { Ionicons } from '@expo/vector-icons';

import Home from "./src/screens/homeScreen";
import Login from "./src/screens/loginScreen";
import About from "./src/screens/aboutScreen"
import Details from "./src/screens/detailScreen";

const stack = createStackNavigator();
const drawer = createDrawerNavigator();
const tab = createBottomTabNavigator();

const homeStack = () => (
    <stack.Navigator>
        <stack.Screen name="Home" component={Home} options={{ headerShown: false }}/>
        <stack.Screen name="Details" component={Details} options={{ headerShown: false }}/>
    </stack.Navigator>
)

const homeScreen = () => (
    <tab.Navigator initialRouteName="Home">
        <tab.Screen name="Home" component={homeStack} options={{tabBarIcon: () => (
           <Ionicons name="md-home" size={30} color='pink' />
           )}}/>
        <tab.Screen name="About" component={About} options={{tabBarIcon: () => (
           <Ionicons name="md-help" size={30} color='pink' />
           )}}/>
    </tab.Navigator>
)

export default () => (
    <NavigationContainer >
        <stack.Navigator initialRouteName="Login">
            <stack.Screen name="Login" component={Login} options={{ headerShown: false }}/>
            <stack.Screen name="Home" component={homeScreen} options={{ headerShown: false }}/>
        </stack.Navigator>
    </NavigationContainer>
);