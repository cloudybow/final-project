import {FETCH_FAILED,FETCH_REQUEST,FETCH_SUCCESS} from "../constants/actionType";

const initialState = {
    isLoading:false,
    data:[],
    isError:{}
}

const fetchReducer = (state=initialState, action) => {
    switch(action.type){
        case FETCH_REQUEST :
            return {
                ...state,
                isLoading:true,
                isError:false
            }
        case FETCH_SUCCESS :
            return {
                ...state,
                isLoading:false,
                isError:false,
                data:[
                    ...state.data,
                    action.payload
                ]
            }
        case FETCH_FAILED :
            return {
                ...state,
                isLoading:false,
                isError:true
            }
        default :
            return state;
    }
}

export default fetchReducer;