import {combineReducers} from "redux";
import loginReducer from "./loginReducer";
import fetchReducer from "./fetchReducer";

const reducer = combineReducers({
    login:loginReducer,
    fetch:fetchReducer
})

export default reducer;