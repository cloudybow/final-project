import {LOGIN} from "../constants/actionType";

const initialState = {
    isTrue:false
}

const loginReducer = (state=initialState, action) => {
    switch(action.type){
        case LOGIN :
            if(action.payload == 'admin'){
                return {isTrue:true}
            } else {
                return {isTrue:false}
            }
        default :
            return state;
    }
}

export default loginReducer;