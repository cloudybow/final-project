import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, StatusBar, ScrollView } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

class AboutScreen extends Component {
    render(){
        return (
            <View style={styles.container}>
              <ScrollView showsVerticalScrollIndicator={false}>
                <StatusBar barStyle={'default'} translucent={false} /> 
                <Text style={{textAlign:'center',fontSize:40,color:'pink',marginTop:10,fontWeight:"bold"}}>ABOUT</Text>
                
                <View style={styles.profile}>
                  <Image source={require('../images/profile.jpg')} style={{width:100,height:100,borderRadius:60,marginTop:20}}/>
                  <Text style={styles.profileText}>Afiyah S. Arief</Text>
                  <Text style={{color:'pink'}}>University Student</Text>
                </View>

                <View style={styles.skillContainer}>
                  
                    <View style={styles.skill}>
                      <Text style={{fontSize:20, color:'pink',marginTop:5,fontWeight:'bold'}}>This App</Text>
                        
                        <View style={{flexDirection:'row', justifyContent:'space-around'}}>
                          <View style={styles.skillIco}>
                            <Text style={{color:'white'}}>This App Shows Details On Some Anime. Click On One of The List To Get The Details!</Text>
                          </View>
                        </View>
                    </View>      

                    <View style={styles.skill}>
                      <Text style={{fontSize:20, color:'pink',marginTop:5,fontWeight:'bold'}}>Programming</Text>
                        
                        <View style={{flexDirection:'row', justifyContent:'space-around'}}>
                          <View style={styles.skillIco}>
                          <MaterialCommunityIcons name="language-cpp" color='pink' size={30}/>
                            <Text style={{color:'white'}}>C++</Text>
                            <Text style={{fontSize:18,color:'white'}}>70%</Text>
                          </View>

                          <View style={styles.skillIco}>
                          <MaterialCommunityIcons name="language-java" color='pink' size={30}/>
                            <Text style={{color:'white'}}>Java</Text>
                            <Text style={{fontSize:18,color:'white'}}>65%</Text>
                          </View>

                          <View style={styles.skillIco}>
                          <MaterialCommunityIcons name="language-javascript" color='pink' size={30}/>
                            <Text style={{color:'white'}}>Javascript</Text>
                            <Text style={{fontSize:18,color:'white'}}>65%</Text>
                          </View>

                          <View style={styles.skillIco}>
                          <MaterialCommunityIcons name="language-html5" color='pink' size={30}/>
                            <Text style={{color:'white'}}>HTML</Text>
                            <Text style={{fontSize:18,color:'white'}}>60%</Text>
                          </View>
                        </View>
                    </View>
                    
                    <View style={styles.skill}>
                      <Text style={{fontSize:20, color:'pink',marginTop:5,fontWeight:'bold'}}>Environment</Text>
                        
                        <View style={{flexDirection:'row', justifyContent:'space-around'}}>
                          <View style={styles.skillIco}>
                            <MaterialCommunityIcons name="react" color='pink' size={30}/>
                            <Text style={{color:'white'}}>React Native</Text>
                          </View>
                        </View>
                    </View>

                    <View style={styles.skill}>
                      <Text style={{fontSize:20, color:'pink',marginTop:5,fontWeight:'bold'}}>Technology</Text>                     
                        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                        <View style={{flexDirection:'row', justifyContent:'space-around', marginTop:10}}>
                          
                          <View style={styles.skillIco}>
                            <MaterialCommunityIcons name="gitlab" color='pink' size={30}/>
                            <Text style={{color:'white'}}>Gitlab</Text>
                            <Text style={{fontSize:18,color:'white'}}>@cloudybow</Text>
                          </View>

                          <View style={styles.skillIco}>
                            <MaterialCommunityIcons name="github-box" color='pink' size={30}/>
                            <Text style={{color:'white'}}>Github</Text>
                            <Text style={{fontSize:18,color:'white'}}>@cloudybow</Text>
                          </View>

                          <View style={styles.skillIco}>
                            <MaterialCommunityIcons name="visual-studio-code" color='pink' size={30}/>
                            <Text style={{color:'white'}}>Visual Studio Code</Text>
                          </View>
                        </View>
                        </ScrollView>
                    </View>   
                </View>

                </ScrollView>
                <Image source={require('../images/logo.png')} style={styles.image}/>
            </View>
        );
    }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DDF3F5',
    alignItems: 'center',
  },
  profile: {
    alignItems:'center'
  },
  profileText: {
    textAlign:'center',
    color:'pink',
    fontSize:20,
    marginTop:5,
    fontWeight:"bold"
  },
  skillContainer :{
    flex:1,
    marginTop:30
  },
  skill: {
    width:300,
    height:130,
    backgroundColor:'#A6DCEF',
    marginBottom:20,
    borderRadius:20,
    justifyContent:'space-around',
    alignItems:'center',
    paddingHorizontal:15,
  },
  skillIco: {
    flexDirection:'column',
    alignItems:'center',
    marginBottom:5,
    marginHorizontal:10
  },
  image: {
    position:'absolute',
    width:50,
    height:50,
    right:15,
    bottom:20
  },
})

export default AboutScreen;