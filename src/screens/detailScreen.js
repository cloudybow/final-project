import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

const DetailScreen = ({route}) => {
        let data = route.params.datas
        return (
            <View style={styles.container}>
                <StatusBar barStyle={'default'} translucent={false} />
                <Text style={{fontSize:40,color:'pink',marginTop:10,fontWeight:"bold",marginBottom:20}}>Details</Text>
                <Image style={styles.image} source={{ uri: `${data.image_url}` }}/>
                <View style={{width:300}}>
                  <Text style={{textAlign:'center',fontSize:16,color:'pink'}}>{data.title}</Text>
                </View>
                <View style={styles.descContainer}>
                    <View style={{width:250}}>
                      <Text style={{textAlign:'center',color:'white'}}>{data.synopsis}</Text>
                      <Text style={{textAlign:'center',marginTop:20, color:'white'}}>Go Here for More Info :</Text>
                      <Text style={{textAlign:'center',color:'white'}}>{data.url}</Text>
                    </View>
                </View>
              
                <Image source={require('../images/logo.png')} style={styles.images}/>
            </View>
        );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DDF3F5',
    alignItems: 'center',
  },
  image: {
    width:100,
    height:100,
    borderRadius:50,
    marginBottom:10
  },
  descContainer: {
    marginTop:20,
    padding:15,
    justifyContent:'space-around',
    width:300,
    backgroundColor:'#A6DCEF',
    borderRadius:20
  },
  images: {
    position:'absolute',
    width:50,
    height:50,
    right:15,
    bottom:20
  }
})

export default DetailScreen;