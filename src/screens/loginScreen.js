import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Image, StatusBar} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import { LOGIN } from '../redux/constants/actionType';

class LoginScreen extends Component {
  constructor(props){
    super(props)
    this.state = {
      user:'',
      pass:''
    }
  }

  loginHandler() {
    this.props.dispatch({
      type:LOGIN,
      payload:this.state.pass
    })
  }

   render(){
      const {login} = this.props;
      if(login.isTrue == true){
        this.props.navigation.navigate('Home')
      }
       return(
         <View style={styles.container}>
             <StatusBar barStyle={'default'} translucent={false} /> 
             <View style={styles.header}>
                <Text style={{fontSize:40,color:'pink',fontWeight:'bold'}}>LOGIN</Text>
             </View>

             <View style={styles.contentContainer}>
                <View style={styles.content}>
                  <Text style={{color:'pink',fontSize:15}}>Username :</Text>
                  <TextInput style={styles.textinput}
                             placeholder='Masukkan Username'
                             placeholderTextColor='white'
                             onChangeText={user => this.setState({user:user})}
                             textAlign={'center'}/>
                </View>
                <View style={styles.content}>
                  <Text style={{color:'pink',fontSize:15}}>Password :</Text>
                  <TextInput style={styles.textinput}
                             placeholder='Masukkan Password'
                             placeholderTextColor='white'
                             onChangeText={pass => this.setState({pass:pass})}
                             secureTextEntry={true}
                             textAlign={'center'}/>
                </View>
             </View>
             
             <View style={styles.buttonContainer}>
              <Button color="pink" title="Login" onPress={()=>this.loginHandler()}/>
             </View>

             <Text style={{fontSize:12,marginTop:10,color:'white'}}>Use admin & admin To Enter!</Text>
             <Image source={require('../images/logo.png')} style={styles.image}/>
         </View>
       );
   }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#DDF3F5',
      alignItems:'center'
    },
    header:{
      marginTop:50
    },
    contentContainer: {
      marginTop:80,
    },
    content:{
      alignItems:'center',
      marginTop:30
    },
    textinput: {
      marginTop:10,
      backgroundColor:'#A6DCEF',
      borderRadius:20,
      color:'white',
      height:30,
      width:200
    },
    image: {
      position:'absolute',
      width:50,
      height:50,
      right:30,
      bottom:40
    },
    buttonContainer:{
      marginTop:100,
      width:100,
    },
});

const mapToState = (state) => (
  {
    login:state.login
  }
)

export default connect(mapToState)(LoginScreen);