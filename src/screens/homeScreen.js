import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList, ActivityIndicator, Image, TouchableOpacity} from 'react-native';
import Axios from 'axios';

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datas: {},
      isError:false,
      isLoading:true
    };
  }

  componentDidMount() {
    this.getData()
  }

  getData = async () => {
    try {
      const response = await Axios.get(`https://api.jikan.moe/v3/search/anime?q=uta%20no%20prince%20sama`)
      this.setState({ isError: false, isLoading: false, datas: response.data })
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

    render(){
      if (this.state.isLoading) {
        return (
          <View
            style={{ alignItems: 'center', justifyContent: 'center', flex: 1}}
          >
            <ActivityIndicator size='large' color='red' />
          </View>
        )
      }
      else if (this.state.isError) {
        return (
          <View
            style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
          >
            <Text>Terjadi Error Saat Memuat Data</Text>
          </View>
        )
      }

        return (
            <View style={styles.container}>
                <StatusBar barStyle={'default'} translucent={false} /> 
                <Text style={{textAlign:'center',fontSize:40,color:'pink',marginTop:10,fontWeight:"bold"}}>HOME</Text>
                <Text style={{textAlign:'center',fontSize:16,color:'pink',marginTop:10,marginBottom:20}}>See The List Below!</Text>
                <FlatList
                  data={this.state.datas.results}
                  renderItem={({ item }) =>
                  <TouchableOpacity onPress={()=>this.props.navigation.navigate('Details',{datas:item})}>
                    <View style={styles.viewList}>
                      <View>
                        <Image source={{ uri: `${item.image_url}` }} style={styles.Image} />
                      </View>
                      <View style={{width:200, marginLeft:10}}>
                        <Text style={{color:'white', fontSize:15}}>{item.title}</Text>
                        <Text style={{color:'white'}}>Episodes : {item.episodes}</Text>
                      </View>
                    </View>
                    </TouchableOpacity>
                  }
                  keyExtractor={(item) => item.mal_id.toString()}
                />
                <Image source={require('../images/logo.png')} style={styles.image}/>
            </View> 
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#DDF3F5',
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewList: {
    height: 100,
    flexDirection: 'row',
    borderWidth: 1,
    borderRadius:20,
    borderColor:'#DDF3F5',
    marginBottom:10,
    alignItems: 'center',
    width: 320,
    backgroundColor:'#A6DCEF',
    paddingHorizontal:20,
    paddingVertical:20
  },
  Image: {
    width: 88,
    height: 80,
    borderRadius: 40
  },
  image: {
    position:'absolute',
    width:50,
    height:50,
    right:15,
    bottom:20
  },
})

export default (HomeScreen);